var AWS = require('aws-sdk');
var _ = require('lodash');
var fs = require('fs');
var async = require('async');
var JSZip = require("jszip");
var config = require('./config/env/development');

var bucket = config.aws.bucket;

AWS.config.update(config.aws.config);

var s3 = new AWS.S3(),
    zip = new JSZip();
var files = [	
	{"type": "RG_09_25","file": "DHIMANT_PATEL-1466126389127.doc"}, 
	{"file": "Hetal_DaveC2C-1466126389080.docx","type": "RG_10_02"}, 
	{"type": "RG_10_09","file": "Kamal_Bhatia-1466126389121.docx"}
];
async.eachLimit(files,10,function(doc,next){
	s3.getObject({Bucket:bucket,Key:'Timesheet/'+doc.file},function(err, data) {
	  if (err){
	  	next('Error while fetching file');
	  } else {
	  	zip.file(doc.file.replace(/-\d*./,'.'),data.Body);
		next();	
	  }
	});
},function(err,data){
	if(err){
		console.log('Error:: '+err);	
	} else {
		zip
		.generateNodeStream({type:'nodebuffer',streamFiles:true})
		.pipe(fs.createWriteStream('public/out.zip'))
		.on('finish', function () {
		    // JSZip generates a readable stream with a "end" event,
		    // but is piped here in a writable stream which emits a "finish" event.
		    console.log("out.zip written.");
		});
	}
});