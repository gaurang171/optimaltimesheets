'use strict';
angular.module('core').directive('oiArrayfield', function() {
	return {
		restrict: 'A',
		scope: {
			data: '=model',
			title: '@',
			fields:'=?',
			lists:'=?'
		},
		controller: ['$scope',function($scope){
			// check if it was defined.  If not - set a default
			$scope.fields = $scope.fields || [{name:'description',type:'text'},{name:'value',type:'text'}];
			$scope.addItem=function(){
				if($scope.data){
					$scope.data.push({});
				} else {
					$scope.data=[{}];
				}
			};
			$scope.removeItem = function(ind){
				$scope.data.splice(ind,1);
			};
		}],
		templateUrl: 'modules/core/utils/arrayField.html'
	};
});