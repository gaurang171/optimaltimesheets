'use strict';
/* globals moment,_*/
//TODO: Moment.js should be dependency not global variable
// Timesheets controller
angular.module('timesheets').controller('TimesheetsController', ['$scope', '$stateParams', '$location', '$window', '$upload', '$modal', '$http', 'Timesheets', 'TimesheetUtils',
	function($scope, $stateParams, $location, $window, $upload, $modal, $http, Timesheets, TimesheetUtils) {

		var setTSParams = function(ts){
            $scope.fileName = $scope.user.displayName.replace(/\s/g,'') + '_' + moment(ts.start).format('MMM-DD') + '_' + moment(ts.end).format('MMM-DD');
            if($scope.isAdmin){
				ts.readonly = false;
				return;
			}
			switch(ts.status){
				case 'Approved':
				case 'Submitted':
				case 'ReturnRequested':
					ts.readonly = true;
					break;
			}
		};

		// Update existing Timesheet
		$scope.update = function() {
			var timesheet = $scope.timesheet ;
			timesheet.$update(function() {
				$location.path('timesheets/' + timesheet._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Timesheets
		$scope.find = function() {
			$scope.timesheets = Timesheets.query();
		};

		// Find existing Timesheet
		$scope.findOne = function() {
			$scope.timesheet = Timesheets.get({ 
				timesheetId: $stateParams.timesheetId
			});
		};

        $scope.getTotal = function(daily){
            var result = 0;
            if(daily){
                for(var i=0;i<daily.length;i++){
                    var tmp = parseFloat(daily[i].hours);
                    if(!tmp){
                        tmp = 0;
                    }
                    result += tmp;
                }
            }
            return result;
        };
        $scope.get = function(){
            $scope.timesheetData = TimesheetUtils.timesheets[$stateParams.timesheetId];
            if($scope.timesheetData) {
                $scope.timesheet = $scope.timesheetData.timesheet;
                $scope.user = $scope.timesheetData.user;
                $scope.project = $scope.timesheetData.project;
                delete $scope.user.timesheets;
                delete $scope.user.projects;
                delete $scope.project.timesheets;

                var timesheet = $scope.timesheet;
                timesheet.user = $scope.user._id;
                timesheet.project = $scope.project._id;

                if(!timesheet.daily){
                    timesheet.daily = [];
                    var start=moment(timesheet.start),end=moment(timesheet.end),current=start, tmp;
                    while(start.valueOf()<=end.valueOf()){
                        timesheet.daily.push({date:start.format('YYYY-MM-DD')});
                        start.add(1,'day');
                    }
                }
                setTSParams($scope.timesheet);
            } else if($stateParams.timesheetId.length === 24){
                $scope.timesheet = Timesheets.get({
                    timesheetId: $stateParams.timesheetId
                },function(data){
                    //success
                    $scope.user = data.project.user;
                    $scope.project = data.project;
                    $scope.timesheet.user = $scope.user._id;
                    $scope.timesheet.project = $scope.project._id;

                    setTSParams($scope.timesheet);

                });
            } else {
                console.log('Error TS97');
            }
        };
        $scope.goBack = function(){
            $window.history.back();
        };
        $scope.save = function(status){
            $scope.timesheet.status = status;
            $scope.timesheet = new Timesheets($scope.timesheet);
            if($scope.timesheet._id){
                $scope.timesheet.$update($scope.goBack);
            } else {
                $scope.timesheet.$save($scope.goBack);
            }
        };

        $scope.functions = {};
        $scope.functions.upload = function(files){
            if($scope.timesheet._id){
                $upload.upload({
                    url:'timesheets/upload',
                    data: {_id:$scope.timesheet._id, fileNames: files.fileNames.join('/\\')},
                    file: files.files
                }).success(function(data, status, headers, config) {
                    $scope.timesheet = data;
                    files.fileNames = [];
                    files.files = [];
                });
            } else {
                $scope.timesheet = new Timesheets($scope.timesheet);
                $scope.timesheet.status = 'Saved';
                $scope.timesheet.$save(function(){
                    $scope.functions.upload(files);
                });
            }
        };
	}
]);