'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
var TrimmedString = {type:String, trim:true};

/**
 * Vendor Schema
 */
var VendorSchema = new Schema({
    name:{type: String, required:'Please fill name', trim:true},
    website:TrimmedString,
    address: {
        line1: TrimmedString,
        line2:TrimmedString,
        city: TrimmedString,
        zip: TrimmedString,
        state: TrimmedString,
        country: TrimmedString,
		_id:false
    },
    emails: [{
		name:TrimmedString,
		email:{
			type: String,
			trim: true,
			match: [/.+\@.+\..+/, 'Please fill a valid email address'],
			lowercase: true
		},
		_id:false}],
    contactPerson:[{
        role:TrimmedString,
        name:TrimmedString,
        email:{
			type: String,
			trim: true,
			match: [/.+\@.+\..+/, 'Please fill a valid email address'],
			lowercase: true
		},
        phone:TrimmedString,
        extra:TrimmedString,
		_id:false
    }],
    docs: [{
        type: TrimmedString,
        file: TrimmedString,
		s3error: Boolean,
		_id:false
    }],
    extra:Schema.Types.Mixed,
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

VendorSchema.statics.getList = function(callback){
    this.find({},'_id name website').sort('name').exec(function(err,vendors){
        if(err){
            return callback(err);
        } else {
            callback(undefined, vendors);
        }
    });
};



mongoose.model('Vendor', VendorSchema);