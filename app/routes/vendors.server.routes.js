'use strict';

module.exports = function(app) {
	var users = require('../controllers/users.server.controller');
	var vendors = require('../controllers/vendors.server.controller');
	var fileUpload = require('../modules/file.upload');
	var mongoose = require('mongoose');
	var Model = mongoose.model('Vendor');


	// Vendors Routes
	app.route('/vendors')
		.get(users.hasAuthorization(['admin']),vendors.list)
		.post(users.hasAuthorization(['admin']), vendors.create);

    app.route('/vendors/getList')
        .get(users.hasAuthorization(['admin']), vendors.getList);

    // Vendor Upload Routes
    app.route('/vendors/upload')
        .post(users.hasAuthorization(['admin']), fileUpload.files.array('file'), fileUpload.upload(Model));

	app.route('/vendors/download/:modelId/*')
		.get(fileUpload.download(Model));

    app.route('/vendors/deleteDoc')
        .post(users.hasAuthorization(['admin']),fileUpload.deleteDoc(Model));

	app.route('/vendors/:vendorId')
		.get(users.hasAuthorization(['admin']),vendors.read)
		.put(users.hasAuthorization(['admin']), vendors.update)
		.delete(users.hasAuthorization(['admin']), vendors.delete);

	// Finish by binding the Vendor middleware
	app.param('vendorId', vendors.vendorByID);
};